Seafile Updater
================

This is an update notifier for Seafile Client (http://seafile.com).
After user confirmation Seafile Client is automatically updated.

Preparations
============

Version information
-------------------

Upload files `latest.txt` and `file.txt` to the webserver:

* `latest.txt` contains newest version available (eg. 4.0.6; without
  any new lines etc.),
* `file.txt` contains URL to MSI installer of Seafile version given
  in `latest.txt`.

In this example we assume these are available at:

```
http://example.com/latest.txt
http://example.com/file.txt
```

When bumping the version, make sure to upload file.txt to server first.

Building updater & installer
----------------------------

1. Install NSIS 2.x (http://nsis.sourceforge.net/Download).
2. Install Inetc plugin (http://nsis.sourceforge.net/Inetc_plug-in)
   - extract archive to NSIS install directory.
3. Set URLs to `latest.txt` and `file.txt` in `updater.nsi`, in lines:
    
        !define URL_LATEST "http://example.com/latest.txt"
        !define URL_FILE "http://example.com/file.txt"

4. Build `updater.exe` (right click on updater.nsi and select
   "Compile NSIS Script").
5. Build `seafile-updater-installer.exe` (using updaterInstall.nsi).
6. Install Seafile Updater on client machines using
   `seafile-updater-installer.exe`

How it works
============

Both parts (installer & updater) are using Nullsoft Install System (NSIS)
to perform their actions and both require admin privileges.

Updater
-------

1. Updater checks if it is the only instance of updater running
   using mutex `SeafileUpdaterMutex
   (see http://nsis.sourceforge.net/Allow_only_one_installer_instance).
2. Temporary directory is created and file latest.txt is downloaded.
3. MSI database in Windows Registry is read. First the ProductCode
   is located using Seafile UpgradeCode (`8C1DED561F5A94C4E8E70B8A5A6A35C5`)
   - ProductCode is a value in the registry key:
   `HKLM\Software\Classes\Installer\UpgradeCodes\8C1DED561F5A94C4E8E70B8A5A6A35C5`
    
    Then the installed Seafile version is determined using property `ProductName`
	of key: `HKLM\Software\Classes\Installer\Products\PRODUCT_CODE`

4. Installed version is compared against the value from `latest.txt` file.
   If version on a server is newer, then user is asked if he/she wants to
   update Seafile.
5. When user confirms, `file.txt` is downloaded and URL referenced
   by this file is saved to temporary directory as `seafile.msi`.
6. `msiexec` is executed in passive mode:
    
        msiexec /i "path/to/seafile.msi" /qb /passive /norestart

7. Exit code from `msiexec` is used to determine if installation completed
   succesfully.

Installer
---------

1. `updater.exe` and uninstaller are extracted to installation directory.
1. Using `schtasks` command, the task is scheduled to be executed
   as user starting the installation every day at 8:00 AM.
   If computer is off at this time, the task will not be run at all!
1. Start Menu shortcuts are created and uninstaller data is written.
