!include LogicLib.nsh
!include MUI.nsh

!define PRODUCT_NAME "Seafile Updater"
!define PRODUCT_VERSION "1.0"
!define PRODUCT_PUBLISHER "Me"

!define INSTALLSIZE "1000"

Name "${PRODUCT_NAME}"
OutFile "seafile-updater-install.exe"
InstallDir "$PROGRAMFILES\${PRODUCT_NAME}"

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_INSTFILES

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_LANGUAGE "English"

RequestExecutionLevel admin

!macro VerifyUserIsAdmin
	UserInfo::GetAccountType
	Pop $0
	${If} $0 != "admin"
		MessageBox MB_ICONSTOP "Administrator rights are required!"
		SetErrorLevel 740
		Quit
	${EndIf}
!macroend

!macro CheckExitCode ErrorString
	StrCmp $0 0 +2 0
	Abort "${ErrorString} Exit code: $0"
!macroend

Function .onInit
	!insertmacro VerifyUserIsAdmin
	SetShellVarContext all
FunctionEnd

Section "install"
	SetOutPath "$INSTDIR"
	
	# Installation directory
	File "updater.exe"
	WriteUninstaller "uninstall.exe"
	
	# Task Scheduler
	nsExec::ExecToLog 'schtasks.exe /create /f /tn "Seafile Updater" /tr "\"$INSTDIR\updater.exe\"" /sc daily /st 08:00 /rl highest /it'
	Pop $0
	!insertmacro checkExitCode "Failed to create scheduled task."
	
	# Start Menu data
	CreateDirectory "$SMPROGRAMS\${PRODUCT_NAME}"
	CreateShortCut "$SMPROGRAMS\${PRODUCT_NAME}\Check for Seafile updates.lnk" "$INSTDIR\updater.exe"
	CreateShortCut "$SMPROGRAMS\${PRODUCT_NAME}\Uninstall.lnk" "$INSTDIR\uninstall.exe"
	
	# Uninstaller data
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}" "DisplayName" "${PRODUCT_NAME} ${PRODUCT_VERSION}"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}" "DisplayVersion" "${PRODUCT_VERSION}"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}" "DisplayIcon" "$\"$INSTDIR\updater.exe$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}" "Publisher" "${PRODUCT_PUBLISHER}"
	
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}" "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}" "QuietUninstallString" "$\"$INSTDIR\uninstall.exe$\" /S"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}" "InstallLocation" "$\"$INSTDIR$\""
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}" "EstimatedSize" ${INSTALLSIZE}
	
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}" "NoModify" 1
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}" "NoRepair" 1
SectionEnd

Function un.onInit
	!insertmacro VerifyUserIsAdmin
	SetShellVarContext all
FunctionEnd

Section "uninstall"
	# Task Scheduler
	nsExec::Exec 'schtasks.exe /delete /tn "Seafile Updater" /f'
	Pop $0
	!insertmacro checkExitCode "Failed to delete scheduled task. Try to reinstall this program and then uninstall it again."
	
	# Installation directory
	Delete "$INSTDIR\updater.exe"
	Delete "$INSTDIR\uninstall.exe"
	RMDir "$INSTDIR"
	
	# Start menu data
	Delete "$SMPROGRAMS\${PRODUCT_NAME}\Check for Seafile updates.lnk"
	Delete "$SMPROGRAMS\${PRODUCT_NAME}\Uninstall.lnk"
	RMDir "$SMPROGRAMS\${PRODUCT_NAME}"
	
	# Temporary data
	RMDir /r "$TEMP\${PRODUCT_NAME}"
	
	# Uninstaller data
	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
SectionEnd
